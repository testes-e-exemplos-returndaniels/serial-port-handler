#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(){
	
	int size = 4;
	int soma = 0;
	
	int * array  = (int *) malloc(size*sizeof(int));
	
	for(int i = 0; i < size; ++i){
		array[i] = soma;
		soma++;
	}
	for(int i = 0; i < size; ++i){
		cout << array[i] << "\n";
    }
    
    cout << "\n";
    
    free(array);
    
	size = 8;
	soma = 10;
	
	array  = (int *) malloc(size*sizeof(int));
	
	for(int i = 0; i < size; ++i){
		array[i] = soma;
		soma++;
	}
	for(int i = 0; i < size; ++i){
		cout << array[i] << "\n";
    }
	
	system("PAUSE");
	return 0;
}
