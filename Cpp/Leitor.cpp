#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace std;

int main(int argc, char *argv[]){
	
	// Abre arquivo
	ifstream file("arquivo.txt", ios::in|ios::binary|ios::ate);	
	if(file.is_open()){
		file.seekg(0, ios::end);	// Vai pro fianl do arquivo
		int size = file.tellg();	// Retrna o indice do arquivo
		file.seekg(0, ios::beg);	// Vai pro inicio do arquivo
		char character;				// Caractere de buffer
		
		// Cria array em memoria alocada
		char * array  = (char *) malloc(size*sizeof(char));
		
		for(int i = 0; i < size; ++i){
			file.get(character);	// L� caractere
			array[i] = character;	// Igula indice [i] da lista � charactere do get()
			cout << array[i];		//	Imprime indice [i] da lista
		//	cout << character;		// Imprime caractere
		}
		cout << "\n";
		file.close();				// Fecha arquivo
		
	}
	system("PAUSE");				// Mantem terminal aberto
	return 0;
}
