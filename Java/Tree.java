/* Cria uma àrvore membro de um Heap Binomial */
import java.util.ArrayList;

public class Tree{
  Node root;         // Nó raiz
  Node newNode;      // Novo nó
  private Node swap; // Nó temporario
  int degree = -1;        // Grau da árvore

  private ArrayList<Node> back; // Armazena antecessores

  // Construtor
  Tree()
  {
    root = null;
    degree = 0;
  }

  // Metodo de inserção de Nós
  public void insert(int value)
  {
    degree++;
    if(root == null)
    {
      root = new Node(value);
    } else {
      order(value);
    }
  }

  /* Escolhe a hierarquia da árvore
   * Dentro do while:
   * Passo 1: Verifica e o valor do novo nó é inferior ao do nó aramzenado em swap
   * ou se o proximo inten não exista.
   * A) Caso a condição seja satisfeita
   * o nó da swap deve apontar para ele e ele para o proximo, sefor aa primeira tentativa root será igual ao novo nó.
   * senão pecorre a lista de nós anteriores organizando os ponteiros.
   * B) Caso contrario passa ao passo 2.
   * Passo 2: swap passa a ter o valor do nó seguinte e volta ao passo 1.
    Fazer os anteriores apnter para o novo nó
   */
  private void order(int value)
  {
    int min;                // armazena valor do no analizado
    Boolean rolling = true; // condicional do while
    Node tmp;               // nó temporario
    int c = 0;              // contador

    newNode = new Node(value); // novo nó

    // Instancia back
    back = new ArrayList<>();

    // inicializa swap
    swap = root;
    while(rolling)
    {
      min = swap.m_value;
	    back.add(swap);
      if(value < min)
      {
        // Novo nó aponta para o nó em swap
        newNode.nextInsert(swap);
        if(c == 0)
        {
          root = newNode;
        } else {
          climbTree();
        }
		    rolling = false;
      } else {
        swap = swap.m_next;

        if(swap==null)
        {
            climbTree();
            rolling=false;
        }
      }
      c++;
    }
  }
  private void climbTree()
  {
	Node tmp;  // nó temporario

	swap = newNode;

	/* Apresenta falha caso array >= 2
	 * Resolve com @int j=back.size()-2;
     */
    for(int j=back.size()-1; j >= 0; --j)
    {
      tmp = back.get(j);
      tmp.nextInsert(swap);
      swap = tmp;
    }
  }
}
