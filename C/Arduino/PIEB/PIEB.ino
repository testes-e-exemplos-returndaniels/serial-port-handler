#include <stdlib.h>
#include <stdio.h>

int latchPin = 8;  //Pino conectado ao pino 12 do 74HC595 (Latch).
int dataPin = 11;  //Pino conectado ao pino 14 do 74HC595 (Data).
int clockPin = 12; //Pino conectado ao pino 11 do 74HC595 (Clock).

char rx = 0;    // Variavel para conter o caractere recebido

void setup(){
  Serial.begin(9600);   // Define a velocidade da porta como 9600 baud
  Serial.flush();       // Limpa o buffer de recepção serial
  
  //Define os pinos como saída
  pinMode(latchPin,OUTPUT);
  pinMode(dataPin,OUTPUT);
  pinMode(clockPin,OUTPUT);
}

void loop(){
   if (Serial.available() >0){
       rx = Serial.read();      // aramazena dado rebido em rx
       Serial.flush();          // Esvazia o buffer de recuperação serial
  
       if (rx=='A' || rx=='a'){
        shiftWrite(1);
       }
       else if (rx=='B' || rx=='b'){
         shiftWrite(1);
         shiftWrite(2);
       }
       else if (rx=='C' || rx=='c'){
         shiftWrite(1);
         shiftWrite(4);
       }
       else if (rx=='D' || rx=='d'){
         shiftWrite(1);
         shiftWrite(4);
         shiftWrite(5);
       }
       else if (rx=='E' || rx=='e'){
         shiftWrite(1);
         shiftWrite(5);
       }
   }
}
void shiftWrite(int pin){
  int qtdRegistradores = 1; //QUANTIDADE DE REGISTRADORES (74HC595).

  int countShift = 1;
  int data = 256;
  for (int i=1; i<=pin; i++){
    if (data <= 1){
      data = 256;
      countShift++;
    }
    data /= 2;
  }
  digitalWrite(latchPin, LOW);
  for (int i=qtdRegistradores; i>0; i--){
    if (i == countShift){
      shiftOut(data);
    }
    else{
      shiftOut(0);
    }
  }
  digitalWrite(latchPin, HIGH);
}

void shiftOut(byte dataOut){
  //Desloca 8 bits, com o bit menos significativo (LSB) sendo deslocado primeiro, no extremo ascendente do clock.
  boolean pinState;
  digitalWrite(dataPin, LOW); //Deixa o registrador de deslocamento pronto para enviar dados.
  digitalWrite(clockPin, LOW);

  for (int i=0; i<8; i++){ //Para cada bit em dataOut, envie um bit.
    digitalWrite(clockPin, LOW); //Define clockPin como LOW antes de enviar o bit.

    //Se o valor de dataOut E(&) de uma mascara de bits forem verdadeiros, defina pinState como 1 (HIGH).
    if (dataOut & (1<<i)){
      pinState = HIGH;
    }
    else{
      pinState = LOW;
    }

    digitalWrite(dataPin, pinState); //Define dataPin como HIGH ou LOW, dependendo de pinState.
    digitalWrite(clockPin, HIGH); //Envia o bit no extremo ascendente do clock.
  }
  digitalWrite(clockPin, LOW); //Interrompe o deslocamento de dados.
}
