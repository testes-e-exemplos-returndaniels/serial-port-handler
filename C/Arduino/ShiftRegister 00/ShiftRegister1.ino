#include <shiftlib.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <piebmap.h>

/**
 * 
 * Função Shift();
 * 
 *Pino  8 conectado ao pino 12 do 74HC595 (Latch).
 *Pino 11 conectado ao pino 14 do 74HC595 (Data).
 *Pino 12  conectado ao pino 11 do 74HC595 (Clock).
 *Quantidade de 74HC595.
 *
*/

Shift pins(8, 11, 12, 1);

char rx = 0;    // Variavel para conter o caractere recebido

void setup(){
  Serial.begin(9600);   // Define a velocidade da porta como 9600 baud
  Serial.flush();       // Limpa o buffer de recepção serial
}

void loop(){
   if (Serial.available() >0){
      rx = Serial.read();      // Aramazena dado rebido em rx
      rx = toupper(rx);        // Converte rx pra maiuscula
      Serial.flush();          // Esvazia o buffer de recuperação serial
      
      Mapear mp;
      int **myMap;
      int x;
      
      if(rx >= 65 && rx <= 90){
        x = rx-65;
        **myMap = mp.capsMap();
      }
      else if (rx >= 20 && rx <= 47){
        x = rx-20;
        **myMap = mp.symbolMap();
      }
      else if (rx >= 58 && rx <= 64){
        x = rx-10;
        **myMap = mp.symbolMap();
      }
      else if (rx >= 48 && rx <= 57){
        int x = rx-48;
        **myMap = mp.numberMap();
      }
      
              
      for(int i = 1; i <= smyMap[x][0]; i++){
         pins.shiftWrite(myMap[x][i]);
      }
  }
}
