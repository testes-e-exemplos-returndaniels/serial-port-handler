/*---------------------------------------------
 S H I F T O U T  M E G A
 Ampliar o número de saídas do Arduino com
 Registrador de Deslocamento 74HC595
 Fellipe Couto - 19/02/2012
 --------------------------------------------*/
 
/*¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
 - Pino 8 do Arduino ligado ao pino 12 do CI.
 - Pino 11 do Arduino ligado ao pino 14 do CI.
 - Pino 12 do Arduino ligado ao pino 11 do CI.
 - Led 1 = Pino 15 do CI.
 - Led 2 = Pino 1 do CI.
 - Led 3 = Pino 2 do CI.
 - Led 4 = Pino 3 do CI.
 - Led 5 = Pino 4 do CI.
 - Led 6 = Pino 5 do CI.
 - Led 7 = Pino 6 do CI.
 - Led 8 = Pino 7 do CI.
 - Pinos 10 e 16 do CI ligados ao Vcc.
 - Pinos 8 e 13 do CI ligados ao GND.
 - Pino 9 do CI ligado ao pino 14 do próximo CI.
 ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨*/
int latchPin = 8;  //Pino conectado ao pino 12 do 74HC595 (Latch).
int dataPin = 11;  //Pino conectado ao pino 14 do 74HC595 (Data).
int clockPin = 12; //Pino conectado ao pino 11 do 74HC595 (Clock).
 
void setup(){
  //Define os pinos como saída
  pinMode(latchPin,OUTPUT);
  pinMode(dataPin,OUTPUT);
  pinMode(clockPin,OUTPUT);
}
 
void loop(){
  int tempo = 200;
  for (int i=1; i<=30; i++){
    shiftWrite(i);
    delay(tempo);
  }
  for (int i=2; i>0; i--){
    shiftWrite(i);
    delay(tempo);
  }
 
  tempo = 10;
  for (int j=0; j<3; j++){
    for (int i=1; i<=30; i++){
      shiftWrite(i);
      delay(tempo);
    }
    for (int i=2; i>0; i--){
      shiftWrite(i);
      delay(tempo);
    }
    tempo -= 2;
  }
}
 
void shiftWrite(int pin){
  int qtdRegistradores = 1; //QUANTIDADE DE REGISTRADORES (74HC595).
 
  int countShift = 1;
  int data = 256;
  for (int i=1; i<=pin; i++){
    if (data <= 1){
      data = 256;
      countShift++;
    }
    data /= 2;
  }
 
  digitalWrite(latchPin, LOW);
  for (int i=qtdRegistradores; i>0; i--){
    if (i == countShift){
      shiftOut(data);
    }
    else{
      shiftOut(0);
    }
  }
  digitalWrite(latchPin, HIGH);
}
 
void shiftOut(byte dataOut){
  //Desloca 8 bits, com o bit menos significativo (LSB) sendo deslocado primeiro, no extremo ascendente do clock.
  boolean pinState;
  digitalWrite(dataPin, LOW); //Deixa o registrador de deslocamento pronto para enviar dados.
  digitalWrite(clockPin, LOW);
 
  for (int i=0; i<8; i++){ //Para cada bit em dataOut, envie um bit.
    digitalWrite(clockPin, LOW); //Define clockPin como LOW antes de enviar o bit.
 
    //Se o valor de dataOut E(&) de uma mascara de bits forem verdadeiros, defina pinState como 1 (HIGH).
    if (dataOut & (1<<i)){
      pinState = HIGH;
    }
    else{
      pinState = LOW;
    }
 
    digitalWrite(dataPin, pinState); //Define dataPin como HIGH ou LOW, dependendo de pinState.
    digitalWrite(clockPin, HIGH); //Envia o bit no extremo ascendente do clock.
  }
  digitalWrite(clockPin, LOW); //Interrompe o deslocamento de dados.
}
